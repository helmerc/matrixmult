# README #

### What is this repository for? ###

* Multi-threaded matrix multiplication
* Version 1

### How do I get set up? ###

* Run main.c
* Will generate matrices automatically and complete the multiplication, displaying metrics relevant to the run.

### Authors ###

* Christopher
* Matt Alden

### Who do I talk to? ###

* Christopher
* christopher.helmer@yahoo.com
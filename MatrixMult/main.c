/**
 * TCSS 422 Operating Systems
 * HW2 Matrix multiplication with threading
 *
 * The matrix multiplication done in this code splits up the work between four threads.
 * The work is split up by rows. if there are 8 rows in A, they are split up as 0-1, 2-3, 4-5, and 6-7.
 * Each thread may be accessing the same column info from B, but no data in C is ever modified simultaneously.
 *
 * Author: Christohper Helmer, Matt Alden
 */

#ifdef __APPLE__
#include <sys/time.h>
#define STOPWATCH_TYPE struct timeval
#define STOPWATCH_CLICK(x) gettimeofday(&x, NULL)
#else
#define _POSIX_C_SOURCE 199309
#include <time.h>
#define STOPWATCH_TYPE struct timespec
#define STOPWATCH_CLICK(x) clock_gettime(CLOCK_REALTIME, &x)
#endif
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>


long ms_diff(STOPWATCH_TYPE start, STOPWATCH_TYPE stop);

/**
 * Holds the two matrices to be multiplied as well as the matrix to put the answer along with other needed
 * information to do the calculations within each thread.
 */
typedef struct {
	const int *A;
	const int *B;
	int *C;
	int row;
	int column;
	int start;
	int stop;
} mat_mult_arg;

/**
 * The thread that will do the actual calculation of the matrix multiplication for the given section
 * The section passed in should handle one fourth of the total calculation.
 */
void matrix_mult_section(mat_mult_arg * matrArg) {

	int i, j, k;
	printf("Calculating rows %d to %d\n", matrArg->start, matrArg->stop);

	//for each of the rows this thread is meant to handle
	for(i = matrArg->start; i <= matrArg->stop; i++) {
		//for the length of the row
		for(j = 0; j < matrArg->row; j++) {
			//set C[i][j] to 0
			matrArg->C[i * matrArg->row + j] = 0;
			for(k = 0; k < matrArg->column; k++) {
				//C[i][j] += A[j][k] * B[k][j]
				matrArg->C[i * matrArg->row + j]
						   += matrArg->A[matrArg->start * matrArg->column + k]
										 * matrArg->B[j + k * matrArg->row];
			}
		}
	}
	printf("Rows %d to %d finished\n", matrArg->start, matrArg->stop);
}

/**
 * initializes the multi-threaded matrix multiplication.
 */
int * multithreaded_matrix_product(const int A[], const int B[], int r, int s, int t) {
	//does not allow multiplication of matrices where the number of rows of matrix A are not equal to the number of columns of matrix B
	if(r != t) {
		printf("Cannot multiply these matrices.\n");
		return NULL;
	}
	//Initialize the output matrix.
	int size = r * t, num_threads = 4, i;
	int *C = (int *)malloc(size * sizeof(int));
	//Create arguments for each thread
	mat_mult_arg matrArg[num_threads];
	for(i = 0; i < num_threads; i++) {
		matrArg[i].A = A;
		matrArg[i].B = B;
		matrArg[i].C = C;
		matrArg[i].row = r;
		matrArg[i].column = s;
		matrArg[i].start = i * r / num_threads;
		matrArg[i].stop = (i + 1) * r / num_threads - 1;
	}
	//create and start the threads
	pthread_t threads[num_threads];
	for(i = 0; i < num_threads; i++) {
		if(pthread_create(&threads[i], NULL, matrix_mult_section, &matrArg[i]) != 0) {
			printf("ERROR: Unable to create new thread.\n");
			exit(EXIT_FAILURE);
		}
	}
	//close all threads after they're done
	for(i = 0; i < num_threads; i++) {
		pthread_join(threads[i], NULL);
	}
	return C;
}

// Creates an r-by-s matrix with random elements in the range [-5, 5].
int * create_random_matrix(int r, int s) {
	int size = r * s;
	int * matrix = (int *)malloc(size * sizeof(int));
	int i;
	for (i = 0; i < size; i++)
		matrix[i] = rand() % 11 - 5;
	return matrix;
}

int main(int argc, char* argv[]) {
	int row = 4, rowCol = 5, column = 4;

	int * Mat1 = create_random_matrix(row, rowCol);
	int * Mat2 = create_random_matrix(rowCol, column);
	int * MatProduct = multithreaded_matrix_product(Mat1, Mat2, row, rowCol, column);

	//print out the two new matrices
	printf("\nMatrix 1:\n");
	printf("%d   ", Mat1[0]);
	for(int i = 1; i < row * rowCol; i++) {
		if(i % rowCol == 0) {
			printf("\n");
		}
		printf("%d   ", Mat1[i]);
	}
	printf("\n\nMatrix 2:\n");
	printf("%d   ", Mat2[0]);
	for(int i = 1; i < rowCol * column; i++) {
		if(i % column == 0) {
			printf("\n");
		}
		printf("%d   ", Mat2[i]);
	}
	printf("\n\nProduct:\n");
	//print out the answer
	printf("%d   ", MatProduct[0]);
	for(int i = 1; i < row * column; i++) {
		if(i % row == 0) {
			printf("\n");
		}
		printf("%d   ", MatProduct[i]);
	}
	printf("\n\n");
	free(Mat1);
	free(Mat2);
	free(MatProduct);
	srand(time(NULL));

	const int r = 1000;
	const int s = 2000;
	const int t = 1000;
	int * A = create_random_matrix(r, s);
	int * B = create_random_matrix(s, t);

	STOPWATCH_TYPE start;
	STOPWATCH_CLICK(start);
	int * C = multithreaded_matrix_product(A, B, r, s, t);
	STOPWATCH_TYPE stop;
	STOPWATCH_CLICK(stop);

	printf("%ld ms elapsed.\n", ms_diff(start, stop));
	free(A);
	free(B);
	free(C);

	return EXIT_SUCCESS;
}

#ifdef __APPLE__
long ms_diff(struct timeval start, struct timeval stop) {
	return 1000L * (stop.tv_sec - start.tv_sec) + (stop.tv_usec - start.tv_usec) / 1000;
}
#else
long ms_diff(struct timespec start, struct timespec stop) {
	return 1000L * (stop.tv_sec - start.tv_sec) + (stop.tv_nsec - start.tv_nsec) / 1000000;
}
#endif
